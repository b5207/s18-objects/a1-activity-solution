function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = 15;

	//methods
	this.tackle = function(target){
		
		if(target.health > 6) {
			console.log(this.name + " tackled " + target.name)
			console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

		let newHealth = target.health -this.attack;
		target.health = newHealth;

		if(newHealth < 5){
			console.log(target.name + " fainted.")
		}

		}else{
			console.warn(target.name + " already fainted.")
		}
	};
};

let bulbasaur = new Pokemon("Bulbasaur", 20);
let charmander = new Pokemon("Charmander", 20);

console.log(bulbasaur);
console.log(charmander);

//bulbasaur.tackle(charmander);
